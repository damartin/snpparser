import numpy as np
from collections import namedtuple
from numpy import exp,pi,angle,absolute,real,imag,log10
import pathlib 
import re
import tkinter as tk
from tkinter import filedialog


def fscale(s):
    # Return frequency scale factor
    return {'KHZ':1e3,'MHZ':1e6,'GHZ':1e9}.get(s.upper(), 1)
        
def flag(f):
    ## Define flags for touchstone format v2
    return {'ver':'[Version]', 
            'np':'[Number of Ports]',
            'porder':'[Two-Port Order]',
            'nf':'[Number of Frequencies]', 
            'nn':'[Number of Noise Frequencies]', 
            'ref':'[Reference]', 
            'matformat':'[Matrix Format]', 
            'mixedmode':'[Mixed-Mode Order]', 
            'begininf':'[Begin Information]', 
            'endinf':'[End Information]', 
            'data':'[Network Data]', 
            'noise':'[Noise Data]', 
            'end':'[End]'}[f] 

def readsNp(fullname=''):
        
    # Read networks data from a touchstone file (.sNp)
    #
    #   obj=READSNP('Filename') parse data from the touchstone 'Filename'.
    #
    # The output argument is a tuple with the following fields:
    #   Name:       name of the touchstone file without path and extension.
    #   Data:       reference impedance
    #   Freq:       frequency vector in Hz
    #   Reference:  matrix of size NxNxlength(Freq)
    #   Type:       type of parameters [S|Y|Z|G|H]
    #   Noise:      noise data in five column format:
    #            <x1> <x2> <x3> <x4> <x5>
    # where
    # x1: Frequency, in the units specified on the option line.
    # x2: Minimum noise figure in decibels (dB).
    # x3: Source reflection coefficient to realize minimum noise figure.  This
    #     is a magnitude regardless of the format specified on the option line.
    # x4: Phase of the reflection coefficient, in degrees.
    # x5: Effective noise resistance.
    #
    # NOTE: MIXEDMODE data still NOT SUPPORTED.
    #
    # Comments on data arranging for n-port networks in Version 2 files follow.
    # From Touchtone File Format Specification (Copyright 2009 by TechAmerica):
    #
    # Rules for Version 2.0:
    # In Version 2.0 files, the data associated with any one frequency may be
    # split across any number of lines or may be placed on a single line of
    # arbitrary length. Network data in a Version 2.0 file is parsed using the
    # [Number of Ports] keyword and argument and the [Matrix Format] keyword
    # and argument.
    #
    # For a Full matrix, a new frequency point is expected every 2n^2 + 1
    # values, # where n is the number of ports, regardless of intervening line
    # termination sequences or characters.  For a Lower or Upper matrix, a new
    # frequency point is expected every n^2 + n + 1 values.
    #
    #
    # Structure of the touchstone files (text after ! are comments):
    #
    # =========================================================================
    # =========================================================================
    #
    # [Version] 2.0                   !required for v2. consider v1.1 otherwise
    # # (option line)                 !required
    #
    # ! For 1-port files: # [Hz|kHz|MHz|GHz] [S|Y|Z] [DB|MA|RI] [R n]
    # ! For 2-port files: # [Hz|kHz|MHz|GHz] [S|Y|Z|G|H] [DB|MA|RI] [R n]
    # ! For 3-port and beyond: # [Hz|kHz|MHz|GHz] [S|Y|Z] [DB|MA|RI] [R n]
    # ! For mixed-mode files: # [Hz|kHz|MHz|GHz] [S|Y|Z] [DB|MA|RI] [R n]
    #
    # ! Note that H- and G-parameters are defined for 2-port networks only.
    # ! These hybrid parameters cannot be used to describe networks containing
    # ! any other number of ports.
    #
    # ! For touchstone versions below v2 all the following flag are ignored:
    #
    # [Number of Ports]               !required for v2.
    # [Two-Port Order]                !required if a 2-port system is described
    # [Number of Frequencies]         !required
    # [Number of Noise Frequencies]   !required if [Noise Data] defined
    # [Reference]                     !optional
    # [Matrix Format]                 !optional
    # [Mixed-Mode Order]              !optional
    # [Begin Information]/[End Information]   !(optional
    #
    # [Network Data]                     !beginning of network data
    #
    # !Data in row format (for 1-port or 2-ports networks)
    # !   -------------------------------------------------
    #     f s11.1 s11.2 s21.1 s21.2 s12.1 s12.2 s22.1 s22.2
    # !   -------------------------------------------------
    #
    # !Data in matrix format (for 3-ports networks and above)
    # !    -------------------------------------------------
    #      f s11.1 s11.2 s12.1 s12.2 ... s1x.1 s1N.2,...
    #        s21.1 s21.2 s22.2 s22.2 ... s2x.1 s2N.2,...
    #        ...',...
    #        sN1.1 sN1.2 sN2.1 sN2.2 ... sNN.1 sNN.2,...
    # !    -------------------------------------------------
    #
    # [Noise Data]   !required only if [Number of Noise Frequencies] given
    #
    # ! Noise data is place after the [Noise Data] flag in five column format:
    #
    #  <frequency> <x1> <x2> <x3> <x4>
    #
    # [End]          !end of the file
    #
    # =========================================================================
    # =========================================================================
    #
    # 28.12.2016, Function readsNp version 3:  Added support for touchstone
    # format Version 2.0 (files starting with the flag [Version] 2.0)
    #
    # Function readsNp version 2: Read data from a touchstone file v1.1
    # (not working with a touchstone v2) to construct a "touchstone" object.
    #
    #
    # 
    # 
    # 
    # 
    # 
    # See also: WRITESNP
    #
    # Author: David Martinez Martinez <davidmartinezmartinez@protonmail.com>
        
    if fullname=='':
        root = tk.Tk()
        root.withdraw()
        fullname = filedialog.askopenfilename()
    
    # Create a pathlib object
    filename = pathlib.Path(fullname)
    
    # Get file extension
    extension = filename.suffix

    ## Get the port number, coded in the file extension
    N=int(extension[2:-1]) 

    ## Read full file and save it to a list. Close file afterwards.
    with open(fullname) as file:
        text = np.loadtxt(file,dtype='str',comments='!',delimiter='\n')

    ## Define global list iterator to iterate over the list of lines
    lineind = 0

    ## read first line
    # In the touchstone v1.1 option line should be the first uncommented line
    # In touchstone v2 the option line come after the flag '[Version] 2.0'
    def readline(text,ind):
        line = text[ind]
        ind = ind+1
        return line,ind
    [firstline,lineind] =readline(text,lineind)

    ## seek [Version] flag
    # If version flag is not present, version 1 is considered
    if flag('ver') in firstline:
        version=int(firstline.split(flag('ver'))[1])
        # Read next line
        [options,lineind]=readline(text,lineind)
    else:
        version=1  # [Version] flag not found. consider version 1
        options=firstline

    ## check option
    assert options[0]=='#', 'Option line or [Version] flag not found.'

    ## Convert option line to uppercase string
    cline = options.upper()

    # NOTE: Parameter in the option line does not follow a given order (appart
    # from the # mark at the beginning and the impedance at the end. It makes
    # necessary to search for them since we do not know were they are a priori.

    ## Check touchstone type 
    type = re.findall('\s([SYZHGT])\s',cline)
    assert len(type)==1, 'Multiple or missing type statements in options line'      
        
    ##  Find the frequency scaling factor.
    unit = re.findall('\s([GMK]*HZ)',cline)
    assert len(unit)==1, 'Multiple or missing frequency units in options line'      

    ## Find the format of network parameters
    DataFormat = re.findall('\s(RI|MA|DB)\s',cline)
    assert len(unit)==1, 'Multiple or missing format statement in options line'    

    ## Find Z0.
    Z_0 = re.findall('\sR\s([\-\+0-9e\.]+)',cline)
    Z_0 = 50 if len(Z_0)==0 else float(Z_0[0])


    ## Initialize additional flags
    porder='21_12' 
    matformat='Full' 
    nfp=[] 
    nnf=[] 

    ## Read additional flags (only for touchstone version 2 and above)
    if version >=2:
        [flagline,lineind]=readline(text,lineind)
        
        while not flag('data') in flagline:
            # the flags may appear in any order relative to each other.
            
            if flag('np') in flagline: # Number of ports
                N = int(flagline.split(flag('np'))[1])
                assert N>=1, 'Value of '+flag('np')+' not valid.'
                
            elif flag('porder') in flagline: # ports order
                porder = '12_21' if '12_21' in flagline else porder
                
            elif flag('nf') in flagline: # number of frequencies
                NF = int(flagline.split(flag('nf'))[1])
                assert NF>=1, 'Value of '+flag('nf')+' not valid.'   
                
            elif flag('nn') in flagline: # number of noise frequencies
                NN = int(flagline.split(flag('nn'))[1])
                assert NN>=0, 'Value of '+flag('nn')+' not valid.'    
                
            elif flag('ref') in flagline: # Reference impedance 
                # [Reference] flag and its arguments may span multiple lines
                ref = flagline.split(flag('ref'))[1]
                Z_0 = [float(i) for i in ref.split()]
                if len(Z_0)>1 and len(Z_0)<N:
                    # Not enough reference values, search in next lines
                    while len(Z_0)<N:
                        [nextline,lineind]=readline(text,lineind)
                        # verify that nextline does not contain any flag
                        assert nextline.strip()[0]!='[', 'Value of '+flag('ref')+' not valid.'
                        ref = nextline.split()
                        Z_0 = Z_0 + [float(i) for i in ref]    
                            
            elif flag('matformat') in flagline: # Matrix format. Default: Full
                matformat = re.findall('((Lower)|(Upper)|(Full))',cline)
                assert len(matformat)==1, 'Invalid '+flag('matformat')+' value'    
                
            # end of while statement. read next line
            [flagline,lineind]=readline(text,lineind)

    ## Compute the number of data fields for each frequency (data may expand 
    # several lines)
    # For a Full matrix, a new frequency point is expected every 2n^2+1 values,
    # where n is the number of ports regardless of intervening line termination
    # sequences or characters.  For a Lower or Upper matrix, a new frequency
    # point is expected every n^2 + n + 1 values.
    Col = 2*N*N + 1 if matformat=='Full' else   N*N + N + 1
       
    ## Process full data. 
    if version==1:
        # Concatenate  string
        st = ' '.join(x for x in text[lineind:])  
        # create float array
        DATA = np.fromstring(st,dtype=float,sep=' ')
        # In version 1 noise starts where freq decreases for the first time
        end_data=np.where(DATA[0:-Col:Col]<DATA[Col::Col])
        ind = len(DATA) if len(end_data[0])==0 else (end_data[0][-1]+2)*Col
        NOISE = DATA[ind:]
        DATA  = DATA[0:ind]   
    else:
        assert text[-1:][0].strip()==flag('end'),'Missing '+flag('end')+' flag'
        text = text[0:-1]   
        # Concatenate  string
        st = ' '.join(x for x in text[lineind:])  
        # Split data and noise for version 2
        (st,noise) = st.split(flag('noise'))
        # create data and noise float array
        NOISE = np.fromstring(noise,dtype=float,sep=' ')
        DATA = np.fromstring(st,dtype=float,sep=' ')
        
    ## Format noise data    
    nnf = len(NOISE)
    if nnf>0:
        assert len(NOISE)%5==0, 'Invalid noise data. Noise data must be formatted as a 5 column table'
        NOISE = NOISE.reshape((int(len(NOISE)/5),5))
        

    ## Read frequency values. One value each "Col" entries
    freq = DATA[0:-1:Col]
    nfp = len(freq)
    # Remove frequency values
    DATA = np.delete(DATA, np.arange(0,len(DATA),Col), axis=None)
    
    ## Process network parameters
    # Compute data with the correspondent units
    if DataFormat[0]=='RI':
        real=DATA[0::2] # Real part of S-parameters
        imag=DATA[1::2] # Imaginary part of S-parameters
        Param=real+1j*imag
    elif DataFormat[0]=='MA':
        module=DATA[0::2] # Module of S-parameters (linear)
        phase=DATA[1::2] # Phase of S-parameters (degrees)
        Param=module*exp(1j*phase*pi/180)
    elif DataFormat[0]=='DB':
        module=DATA[0::2] # Module of S-parameters (decibels)
        phase=DATA[1::2] # Phase of S-parameters (degrees)
        Param=np.power(10,(module/20))*exp(1j*phase*pi/180)

    ## Complete data if upper or lower triangular matrix and reshape
    ## scattering parameters into a 3d matrix of dimension nfpxNxN
    if matformat=='Full':
        U = np.reshape(Param,(nfp,N,N))
    elif matformat=='Lower':
        U = np.triu(np.ones((nfp,N,N)))         # Init 3d triangular matrix
        U[U==1]=Param                           # Fill upper triangular matrix
        L = np.triu(np.ones((nfp,N,N)),1)       # upper triangular index
        U += U(L==1).transpose([0,2,1])         # Complete 3d matrix
    elif matformat=='Upper':
        U = np.tril(np.ones((nfp,N,N)))         # Init 3d triangular matrix
        U[U==1]=Param                           # Fill upper triangular matrix
        L = np.tril(np.ones((nfp,N,N)),-1)      # upper triangular index
        U += U(L==1).transpose([0,2,1])         # Complete 3d matrix

    ## Change parameters order unles flag('porder') = '21_12' for .s2p file.
    #U=U if (N==2 and Version==1) else U.transpose([0,2,1])
    if (N==2) and (version==1 or flag('porder') == '21_12'):
        U = U.transpose([0,2,1])

    ## assign output arguments
    Network = namedtuple('Network', ['Name','Freq','Data',
                                     'Reference','Type','Noise'])
    system = Network(filename.stem, freq*fscale(unit[0]), U, Z_0, type, NOISE )
    return system


def writesNp(Name,Data,Freq,Noise=np.array([]),Ref=np.array([50]),Version=1,
             Comment='',Format='RI', Type='S',Scale='GHz'):
    
    # WRITESNP   Write data to a touchstone file v1.1
    #
    #   WRITESNP(obj) write structure data to a file .sNp
    #
    #   WRITESNP(obj,name) write structure data to a file name.sNp
    #
    #   WRITESNP(obj,name,'Name',name) write structure data to a file name.sNp
    #   and format the scattering parameters according to type. Argument
    #   type accept the following options: MA for module and phase, RI for
    #   real and imaginary part, DB for module in dB and phase in deg.
    #
    #   Optional input argument are:
    #
    #   Version: specifies if the touchstone format corresponds to Version 1 or
    #   2. If not specified p.Results.Version 1 is considered.
    #
    #   commentline: comment to be writen at the begining of the  file.
    #
    #
    #
    # 
    # 
    # 
    # 
    # 
    #   See also: WRITESNP
    #
    #   Author: David Martinez <david.martinez@inria.fr>
    

    ## Number of ports (last dimension of data array)
    N=Data.shape[-1]

    # if it's a TwoPort and we use version 1, the order is 21_12
    if (N==2) and (Version==1):
        Data = Data.transpose([0,2,1])
    
    ## Check number of impedance values if a vector is provided
    assert Ref.size==1 or Ref.size==N, 'Number of reference impedances \
        does not match the number of ports'

    ## Check format of noise data
    assert N!=2 or Noise.size==0, 'Noise is only allowed for 2-ports networks'
    assert Noise.shape[0]==0 or Noise.shape[1]==5, 'Noise data must be \
                                                    formmated in 5 columns'
    if len(Noise)>0:
        assert Version==2 or Noise[0][0]<Freq[-1:], 'First noise frequency must be\
                            smaller than last data frequency in touchstone v1'
        
    ## Convert reference values to str
    R = ''
    for i in Ref:
        R += ' '+str(i)

    ## filename
    Name += ('.s'+str(N)+'p')

    ## Number of frequencies
    NF = len(Freq)
    
    ## Number of noise frequencies
    NN = Noise.shape[0]
    
    ## Preallocate touchstone data
    tempdata = np.empty((NF, 1+2*N*N)) * np.nan
    
    ## Fil frequency axis
    tempdata[:,0] = Freq / fscale(Scale)
    if NN > 0:
        Noise[:,0] = Noise[:,0] / fscale(Scale)

    ## Shape data as a 2x2 matrix and fill tempdata object
    Data = Data.reshape((NF,N*N))
    if Format=='RI':
        tempdata[:,1::2]=real(Data) 
        tempdata[:,2::2]=imag(Data) 
    elif Format=='MA':
        tempdata[:,1::2]=absolute(Data) 
        tempdata[:,2::2]=angle(Data)*180/pi 
    elif Format=='DB':
        tempdata[:,1::2]=20*log10(absolute(Data)) 
        tempdata[:,2::2]=angle(Data)*180/pi 

    ## optionline
    optionline = '# '+Scale+' '+Type+' '+Format+' R '+str(Ref[0])

    # comments
    header = ('! Touchstone Version '+str(Version)+'\n'+
              '! Touchstone file generated by writesNp \n'+
              '! Functions "WRITESNP" and "READSNP"  can be found at \n'+
              '! https://gitlab.inria.fr/damartin/TouchstoneParser.git \n!\n')
      
    ## network data
    if N<=2:
        # Write data in row format
        #     -------------------------------------------------
        #     f s11.1 s11.2 s21.1 s21.2 s12.1 s12.2 s22.1 s22.2
        #     -------------------------------------------------
        dataformat = '%f '
    else:
        # Write data in matrix format
        #     -------------------------------------------------
        #      f s11.1 s11.2 s12.1 s12.2 ... s1x.1 s1x.2,...
        #        s21.1 s21.2 s22.2 s22.2 ... s2x.1 s2x.2,...
        #        ...',...
        #        sx1.1 sx1.2 sx2.1 sx2.2 ... sxx.1 sxx.2,...
        #     -------------------------------------------------
        dataformat = '%f \t'+ (N-1)*(2*N*'%f '+'\n        \t') + 2*N*'%f '+'\n'        

    ## add flags for Version 2
    dataheader = flag('data')  if Version==2 else '! Network Data'
    noisheader = flag('noise') if Version==2 else '! Noise Data'
    noisfooter = flag('end')   if Version==2 else '! End'
    
    flags = np.array([flag('ver')+' '+str(Version), optionline, 
                      flag('np')+' '+str(N),
                      flag('porder')+' 12_21',
                      flag('nf')+' '+str(NF),
                      flag('nn')+' '+str(NN),
                      flag('ref')+R]) if Version==2 else np.array([optionline])

    ## Write to file    
    with open(Name,'w') as file:
        file.writelines(header)
        np.savetxt(file,flags,fmt='%s',header=Comment,comments='!')
        np.savetxt(file,tempdata,fmt=dataformat,header=dataheader,comments='')
        np.savetxt(file,Noise,fmt='%f',
                   header=noisheader,footer=noisfooter,comments='')
        

if __name__ == '__main__':
    x = readsNp('superdirective.s1p')
    writesNp('superdirective2',x.Data,x.Freq,Version=2,Format='MA')
    noisedata = np.arange(50).reshape((10,5))
    noisedata[:,0] = noisedata[:,0]*1e9


