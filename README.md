# TOUCHSTONE PARSER
PYTHON methods to PARSE/WRITE SnP files, including version 2.

## Contents:

* READSNP  
* WRITESNP  

--------------------------------------------------------------------------
### READSNP  
Read networks data from a touchstone file (.sNp)

    obj=READSNP('Filename') 
    
The output argument is a tuple with the following fields: 

  * Name:   name of the touchstone file without path and extension.  
  * Zref:   reference impedance  
  * Freq:   frequency vector in Hz  
  * Data:   scattering matrix of size NxNxlength(Freq)  
  * Type:   type of parameters [S|Y|Z|G|H]  
  * Noise:  noise data in five column format:
         ```<x1> <x2> <x3> <x4> <x5>```
  where  
    * x1: Frequency, in the units specified on the option line.  
    * x2: Minimum noise figure in decibels (dB).  
    * x3: Source reflection coefficient to rea lize minimum noise figure. 
      This is a magnitude regardless of the format specified on the option line.  
    * x4: Phase of the reflection coefficient, in degrees.  
    * x5: Effective noise resistance.  

**NOTE: MIXEDMODE data still NOT SUPPORTED.**

--------------------------------------------------------------------------
### WRITESNP   
Write data to a touchstone file   

    WRITESNP(Data, Frequency, Parameter1, value1, Parameter2, value2, ...)
    
where *Data* is a matrix of size `NxNxF` being `N` the number of ports and
*Frequency* is the frequency axis of length `F`.   

Optional input arguments can be passed as a *Parameter-Value* list. These
parameters can be:  

| Parameter | Description   | 
| --------: |:------------- | 
| Version   | The version of the touchtone format. Either 1 or 2.
| Name      | The name of the .sNp file.
| Format    | A character indicating the type of parameters in the *Data* matrix.   Valid values are `S,Y,Z,G or H`  for two-ports objects and `S,Y or Z` for any other number of ports. 
| Type      | Units in which the *Data* will be written. It can be `DB` for decibels, `MA` for magnitude and angle or `RI` for real and imaginary part.
| Ref       | The reference impedance. It can be a vector for touchstone files of version 2. If not specified Version 1 is considered. 
| Scale     | The units of the frequency axis: `Hz,kHz,MHz or GHz`.
| Comment   | Any string to be writen as a comment line in the header.

  
----------------------------------------------------------------------------
  
### Comments on data arranging for n-port networks in Version 2 files follow.  

From Touchtone File Format Specification (Copyright 2009 by TechAmerica):

[https://ibis.org/touchstone_ver2.0/touchstone_ver2_0.pdf](https://ibis.org/touchstone_ver2.0/touchstone_ver2_0.pdf)  

Rules for Version 2.0:  

In Version 2.0 files, the data associated with any one frequency may be
split across any number of lines or may be placed on a single line of
arbitrary length. Network data in a Version 2.0 file is parsed using the
[Number of Ports] keyword and argument and the [Matrix Format] keyword
and argument.  

If the 'Lower'/'Upper' keywords are indicated after the [Matrix Format] 
only the  lower/upper triangle of the matrix data is writen, reducing the 
file size in the case of a simmetric matrix.

For a Full matrix, a new frequency point is expected every 2n^2 + 1
values,  where n is the number of ports, regardless of intervening line
termination sequences or characters.  For a Lower or Upper matrix, a new
frequency point is expected every n^2 + n + 1 values.  


### Structure of the touchstone files (text after ! are comments): 

```
[Version] 2.0                   !required for v2. consider v1.1 otherwise
# (option line)                 !required

! For 1-port files: # [Hz|kHz|MHz|GHz] [S|Y|Z] [DB|MA|RI] [R n]
! For 2-port files: # [Hz|kHz|MHz|GHz] [S|Y|Z|G|H] [DB|MA|RI] [R n]
! For 3-port and beyond: # [Hz|kHz|MHz|GHz] [S|Y|Z] [DB|MA|RI] [R n]
! For mixed-mode files: # [Hz|kHz|MHz|GHz] [S|Y|Z] [DB|MA|RI] [R n]

! Note that H- and G-parameters are defined for 2-port networks only.
! These hybrid parameters cannot be used to describe networks containing
! any other number of ports.

! For touchstone versions below v2 all the following flag are ignored:

[Number of Ports]               !required for v2.
[Two-Port Order]                !required if a 2-port system is described
[Number of Frequencies]         !required
[Number of Noise Frequencies]   !required if [Noise Data] defined
[Reference]                     !optional
[Matrix Format]                 !optional
[Mixed-Mode Order]              !optional
[Begin Information]/[End Information]   !(optional

[Network Data]                     !beginning of network data

!Data in row format (for 1-port or 2-ports networks)
!   -------------------------------------------------
    f s11.1 s11.2 s21.1 s21.2 s12.1 s12.2 s22.1 s22.2
!   -------------------------------------------------

!Data in matrix format (for 3-ports networks and above)
!    -------------------------------------------------
     f s11.1 s11.2 s12.1 s12.2 ... s1x.1 s1N.2,...
       s21.1 s21.2 s22.2 s22.2 ... s2x.1 s2N.2,...
       ...',...
       sN1.1 sN1.2 sN2.1 sN2.2 ... sNN.1 sNN.2,...
!    -------------------------------------------------

[Noise Data]   !required only if [Number of Noise Frequencies] given

! Noise data is place after the [Noise Data] flag in five column format:

 <frequency> <x1> <x2> <x3> <x4>

[End]          !end of the file
``` 
### Requirements
The code has been tested on Python 3.6
