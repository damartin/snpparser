#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 20:20:27 2019

@author: damartin
"""

import unittest
import SNPParser as p
from itertools import product
from collections import namedtuple
import numpy as np
import coverage as cov
import os
import traceback
class TestSNPP(unittest.TestCase):
   
    def __init__(self,testName, par):
        super(TestSNPP, self).__init__(testName)
        Param = namedtuple('Param', ['version',
                                     'n_ports',
                                     'formats',
                                     'mattype',
                                     'f_scale',
                                     'n_freqs'])
        self.args = Param(*par)
    
    def setUp(self):
        self.Freq = np.sort(np.random.rand(self.args.n_freqs))
        self.Freq = np.linspace(1e6,1e9,self.args.n_freqs)
        self.Data = np.random.rand(self.args.n_freqs,self.args.n_ports,self.args.n_ports) * (1+1j)
        self.Name = 'touchstone_readwrite_testfile'
        self.File = self.Name + '.s' + str(self.args.n_ports) + 'p'
 
    def tearDown(self):
        os.remove(self.File)
        pass
    
    def testparse(self):
        try:
            p.writesNp(self.Name, self.Data, self.Freq,
                       Version=self.args.version,
                       Format =self.args.formats,
                       Type   =self.args.mattype,
                       Scale  =self.args.f_scale)
            dataread = p.readsNp(self.File)
        except Exception as e:
            self.fail(traceback.format_exc() + str(self.args))
                
        np.testing.assert_almost_equal(dataread.Data, self.Data, 2,str(self.args))
        np.testing.assert_almost_equal(dataread.Freq, self.Freq, 2,str(self.args))

def suite():
    VERSION = (1,2)
    N_PORTS = (1, 2, 10)
    FORMATS = ('RI', 'MA', 'DB')
    F_SCALE = ('GHZ','MHZ','KHZ','HZ')
    MATTYPE = ('S','Z','Y')
    N_FREQS = (1,10,1000)
    
    testcases = list(product(VERSION, N_PORTS, FORMATS, MATTYPE, F_SCALE, N_FREQS))
    suite = unittest.TestSuite()
    for case in testcases:
        suite.addTest(TestSNPP('testparse',case))
    return suite

if __name__ == '__main__':
    result = unittest.TextTestRunner().run(suite())

